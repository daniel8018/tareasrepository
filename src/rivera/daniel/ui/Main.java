package rivera.daniel.ui;

import rivera.daniel.tl.Controller;

import java.io.*;

/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 *
 */

public class Main {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static Controller admin = new Controller();


    public static void main(String[] args) throws IOException {


        mostrarMenu();

    }//FIN MAIN


    static void mostrarMenu() throws IOException {
        int opcion = -1;
        do {
            System.out.println("******************MENÚ DIRECTORY CLOTHING, INC.****************");
            System.out.println("******CATALOGOS******");
            System.out.println("1. Registrar Catalogo");
            System.out.println("2. Listar Catalogos");


            System.out.println("");
            System.out.println("******CLIENTES******");
            System.out.println("3. Registrar Cliente");
            System.out.println("4. Listar Cliente");

            System.out.println("");
            System.out.println("*******CAMISAS******");
            System.out.println("5. Registrar Camisa");
            System.out.println("6. Listar Camisa   ");
            System.out.println("**************************************************************");

            System.out.println("0. Salir");


            opcion = seleccionarOpcion();
            procesarOpcion(opcion);
        } while (opcion != 0);
    }

    //Rutina (función) que retorna el valor ingresado por el usuario.
    static int seleccionarOpcion() throws IOException {
        System.out.println("Digite la opción");
        return Integer.parseInt(in.readLine());

    }

    //Rutina (procedimiento) que según el parámetro, llama al proceso respectivo.
    static void procesarOpcion(int pOpcion) throws IOException {
        switch (pOpcion) {
            case 0:
                System.out.println("¡Aplicación cerrada exitosamente!.");
                break;
            case 1:
                registrarCatalogo();
                break;
            case 2:
                listarCatalogos();
                break;
            case 3:
                registrarCliente();
                break;
            case 4:
                listarClientes();
                break;
            case 5:
                registrarCamisa();
                break;
            case 6:
                listarCamisas();
                break;
            default:
                System.out.println("Opción inválida");
                break;
        }
    }

    //**************************************REGISTRAR***********************************************+
    static void registrarCliente() throws IOException {
        String nombre, apellido1, apellido2, direccion, correo;

        System.out.println("Digite el nombre del Cliente");
        nombre = in.readLine();
        System.out.println("Digite el primer apellido del cliente");
        apellido1 = in.readLine();
        System.out.println("Digite el segundo apellido del cliente");
        apellido2 = in.readLine();
        System.out.println("Digite la dirección del cliente");
        direccion = in.readLine();
        System.out.println("Digite el correo del cliente");
        correo = in.readLine();
//*************************************************************************************+
        if (admin.ValidarClienteNombre(nombre, apellido1, apellido2, direccion, correo) == null) {
            admin.registrarCliente(nombre, apellido1, apellido2, direccion, correo);
            System.out.println("");
            System.out.println("Listo, Cliente registrado!");
            System.out.println("");                                //_______CLIENTE_________
        } else {
            System.out.println("El Cliente que usted registró ya existe");
            System.out.println("");
        }//FIN ELSE
    }//FIN REGISTRAR CLIENTE

    //*************************************************************************************+
    public static void registrarCatalogo() throws IOException {
        String mes = "mes", fecha = "fecha";
        String year = "";

        System.out.println("REGISTRANDO NOMBRE DEL CATÁLOGO QUE SERÁ MES+ AÑO+ FECHA DE CREACIÓN");
        System.out.println("");
        System.out.println("Ingrese el mes del Catálogo");
        mes = (in.readLine());
        System.out.println("Ingrese el año del catálogo");
        year = (in.readLine());
        System.out.println("Ingrese la fecha de creación del catálogo");
        fecha = (in.readLine());
        System.out.println("");

        if (admin.ValidarCatalogoNombre(mes, fecha, year) == null) {
            admin.registrarCatalogo(mes, fecha, year);
            System.out.println("");
            System.out.println("Listo, Catálogo registrado!");
            System.out.println("\n");                             //_______Catalogo_________
        } else {
            System.out.println("El Catálogo que usted registró ya existe");
            System.out.println("\n");
        }//FIN ELSE
    }//FIN REGISTRAR CATALOGO

    //*************************************************************************************+
    public static void registrarCamisa() throws IOException {
        String id;
        String color, descripcion;
        String size;
        String precio;

        System.out.println("REGISTRANDO VALORES DE LA CAMISA EN EL CATÁLOGO (ID, COLOR, DESCRIPCIÓN, TAMAÑO, PRECIO");
        System.out.println("");
        System.out.println("Ingrese el ID de la camisa");
        id = (in.readLine());
        System.out.println("Ingrese el color de la camisa");
        color = (in.readLine());
        System.out.println("Ingrese la descripción de la camisa");
        descripcion = (in.readLine());
        System.out.println("Ingrese el tamaño de la camisa  (L, M, S)");
        size = (in.readLine());
        System.out.println("Ingrese el precio de la camisa");
        precio = (in.readLine());

        if (admin.ValidarCamisaNombre(id, color, descripcion, size, precio) == null) {
            admin.registrarCamisa(id, color, descripcion, size, precio);
            System.out.println("");
            System.out.println("Listo, Camisa registrada!");
            System.out.println("\n");                            //_______CAMISA_________
        } else {
            System.out.println("La camisa que usted registró ya existe");
            System.out.println("\n");
        }//FIN ELSE
    }//FIN REGISTRAR CAMISA


    //*******************************************LISTAR******************************************+

    static void listarClientes() {
        String[] listaC = admin.listarClientes();
        for (String info : listaC) {
            System.out.println(info);
        }//FIN for
    }//FIN LISTAR CLIENTES

    static void listarCatalogos() {
        String[] listaCa = admin.listarCatalogos();
        for (String info : listaCa) {
            System.out.println(info);
        }//FIN for
    }//FIN LISTAR CATALOGOS

    static void listarCamisas() {
        String[] listaCam = admin.listarCamisas();
        for (String info : listaCam) {
            System.out.println(info);
        }//FIN for
    }
}

//FIN MAIN-------------------------------------------





