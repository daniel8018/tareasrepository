package rivera.daniel.dl;

import rivera.daniel.bl.Camisa;
import rivera.daniel.bl.Catalogo2;
import rivera.daniel.bl.Cliente2;
import rivera.daniel.tl.Controller;
import java.util.ArrayList;


/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class Capa {
    private  ArrayList<Catalogo2> catalogos;
    private  ArrayList<Camisa> camisas;
    private    ArrayList<Cliente2> clientes;

    /**
     * Constructor que recibe todos los parámetros de  Capa  y los Inicializa
     *
     */
    public Capa() {
        catalogos = new ArrayList<>();
        camisas = new ArrayList<>();
        clientes = new ArrayList<>();

    }// FIN DE CONTRUCTOR.

    public int getvalorClientes(){
        int valor= clientes.size();
        return valor;
    }//FIN GETVALORCLIENTES

    /**Método que proveniente de Arraylist
     *
     * @return Devuelve el contenido del arrayList de Cliente2
     */
    public  ArrayList<Cliente2> getClientes() {
        return clientes;
    }

    /**
     *
     * @param clientes método para ingresar valores al Arraylist de Clientes
     */
    public void setClientes(ArrayList<Cliente2> clientes) {
        this.clientes = clientes;
    }

    /**
     *
     * @param c método para agregar al arraylist clientes el objeto C
     */
    public  void agregarCliente(Cliente2 c) {
        clientes.add(c);
    }

    /**
     *
     * @param ca método para agregar al arraylist clientes el objeto Ca
     */
    public  void agregarCatalogo(Catalogo2 ca) {
        catalogos.add(ca);
    }

    /**
     *
     * @param cam método para agregar al arraylist clientes el objeto Cam
     */
    public  void agregarCamisa(Camisa cam) {
        camisas.add(cam);
    }

    /**
     * Método que valida si los datos que vienen como parámetro coinciden con los del objeto Catálogo
     * @param pmes parámetro mes utilizado para ser comparado con el atributo del objeto
     * @param pfecha parámetro fecha utilizado para ser comparado con el atributo del objeto
     * @param pyear parámetro año utilizado para ser comparado con el atributo del objeto
     * @return devuelve null si no hay coincidencia, o devuelve el parámetro comparado si  la hay
     */
    public String validarCatalogoNombre(String pmes, String pfecha, String pyear){
        for (Catalogo2 validCatalogoNombre : catalogos) {
            if (pmes.equalsIgnoreCase(validCatalogoNombre.getMes())) {
                return validCatalogoNombre.getMes();
            } else if(pfecha.equalsIgnoreCase(validCatalogoNombre.getFecha())) {
                return validCatalogoNombre.getFecha();
            } else if (pyear.equalsIgnoreCase(validCatalogoNombre.getYear())) {
                return validCatalogoNombre.getYear();
            } else {
                return null;
            }//FIN FOR INICIAL
        }
        return null;
    }//FIN VALIDARCATALOGO NOMBRE



    /** Mëtodo que crea un nuevo arreglo con la información de un Arraylist
     *
     * @return  Devuelve la información del nuevo Arraylist
     */
    public String[] getCatalogo() {
        int cont = 0;
        String[] info = new String[catalogos.size()];
        for (Catalogo2 regCatalogos : catalogos) {
            System.out.println("");
            info[cont] = regCatalogos.toStringCatalogo();
            System.out.println("");
            cont++;
        }
        return info;
    }// FIN DE GETCATALOGO.

    /** Método que valida si los datos que vienen como parámetro coinciden con los del objeto Cliente
     *
     * @param pnombre parámetro nombre utilizado para ser comparado con el atributo del objeto
     * @param papellido1 parámetro primer apellido utilizado para ser comparado con el atributo del objeto
     * @param papellido2 parámetro segundo apellido utilizado para ser comparado con el atributo del objeto
     * @param pdireccion parámetro dirección utilizado para ser comparado con el atributo del objeto
     * @param pcorreo parámetro correo utilizado para ser comparado con el atributo del objeto
     * @return devuelve null si no hay coincidencia, o devuelve el parámetro comparado si  la hay
     */

    public String validarClienteNombre(String pnombre,String papellido1, String papellido2,String pdireccion, String pcorreo){
        for (Cliente2 validClienteNombre : clientes) {
            if (pnombre.equalsIgnoreCase(validClienteNombre.getNombre())) {
                return validClienteNombre.getNombre();
            } else if (papellido1.equalsIgnoreCase(validClienteNombre.getApellido1())) {
                return validClienteNombre.getApellido1();
            } else if (papellido1.equalsIgnoreCase(validClienteNombre.getApellido1())) {
                return validClienteNombre.getApellido1();
            } else if (papellido2.equalsIgnoreCase(validClienteNombre.getApellido2())) {
                return validClienteNombre.getApellido2();
            } else if (pdireccion.equalsIgnoreCase(validClienteNombre.getDireccion())) {
                return validClienteNombre.getDireccion();
            } else if (pcorreo.equalsIgnoreCase(validClienteNombre.getCorreo())) {
                return validClienteNombre.getCorreo();
            } else {
                return null;
            }//FIN FOR INICIAL
        }
        return null;
    }//FIN VALIDARCLIENTE NOMBRE

    /**Mëtodo que crea un nuevo arreglo con la información de un Arraylist
     *
     * @return Devuelve la información del nuevo Arraylist
     */

    public String[] getCliente() {    //LISTAR LOS CLIENTES
        int cont = 0;
        String[] info = new String[clientes.size()];
        for (Cliente2 regClientes : clientes) {
            System.out.println("");
            info[cont] = regClientes.toStringCliente();
            System.out.println("");
            cont++;
        }
        return info;
    }// FIN DE GETCLIENTE

    public String validarCamisaNombre(String pid, String pcolor, String pdescripcion, String psize, String pprecio){
        for (Camisa validCamisaNombre : camisas) {
            if (pid.equalsIgnoreCase(validCamisaNombre.getColor())) {
                return validCamisaNombre.getId();
            } else if (pcolor.equalsIgnoreCase(validCamisaNombre.getColor())) {
                return validCamisaNombre.getColor();
            } else if (pdescripcion.equalsIgnoreCase(validCamisaNombre.getDescripcion())) {
                return validCamisaNombre.getDescripcion();
            } else if (psize.equalsIgnoreCase(validCamisaNombre.getSize())) {
                return validCamisaNombre.getSize();
            } else if (pprecio.equalsIgnoreCase(validCamisaNombre.getPrecio())) {
                return validCamisaNombre.getPrecio();
            } else {
                return null;
            }//FIN FOR INICIAL
        }
        return null;
    }//FIN VALIDARCATALOGO NOMBRE

    /**Mëtodo que crea un nuevo arreglo con la información de un Arraylist
     *
     * @return Devuelve la información del nuevo Arraylist
     */
    public String[] getCamisa() {
        int cont = 0;
        String[] info = new String[camisas.size()];
        for (Camisa regCamisa : camisas) {
            System.out.println("");
            info[cont] = regCamisa.toStringCamisa();
            System.out.println("");
            cont++;
        }
        return info;
    }// FIN DE GETCAMISA.


}// FIN DE CLASS


