package rivera.daniel.bl;


/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 *
 */

public class Cliente2 {
    private String nombre, apellido1, apellido2, direccion, correo;

    /**
     * constructor por defecto de la clase Client2
     */
    public Cliente2() {
    }

    /**
     * Constructor que recibe todos los parámetros y los inicializa
     * @param nombre Parámetro que simboliza el nombre de Cliente
     * @param apellido1 Parámetro que simboliza el nombre de Primer apellido
     * @param apellido2 Parámetro que simboliza el nombre de segundo apellido
     * @param direccion Parámetro que simboliza el nombre de Dirección
     * @param correo  Parámetro que simboliza el nombre de correo
     */
    public Cliente2(String nombre, String apellido1, String apellido2, String direccion, String correo) {
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.direccion = direccion;
        this.correo = correo;
    }


    /** Método Get de Nombre
     *
     * @return Parámetro que devuelve el contenido de el parámetro privado Nombre Para Cliente
     */
    public String getNombre() {
        return nombre;
    }

    /** Método Set de Nombre
     *
     * @param nombre Parámetro que se encarga de enviarse para ser el nuevo valor del nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /** Método Get de Primer Apellido
     *
     * @return Parámetro que devuelve el contenido de el parámetro privado Apellido Para Cliente
     */
    public String getApellido1() {
        return apellido1;
    }

    /** Método Set de Apellido1
     *
     * @param apellido1 nombre Parámetro que se encarga de enviarse para ser el nuevo valor de Apellido1
     */
    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }
    /** Método Get de Segundo Apellido
     *
     * @return Parámetro que devuelve el contenido de el parámetro privado de segundo apellido Para Cliente
     */
    public String getApellido2() {
        return apellido2;
    }

    /** Método Set de Apellido2
     *
     * @param apellido2 Parámetro que se encarga de enviarse para ser el nuevo valor del apellido2
     */
    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    /** Método Get de Dirección
     *
     * @return Parámetro que devuelve el contenido de el parámetro privado de Dirección Para Cliente
     */
    public String getDireccion() {
        return direccion;
    }

    /** Método Set de Dirección
     *
     * @param direccion Parámetro que se encarga de enviarse para ser el nuevo valor de Dirección
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /** Método Get de Correo
     *
     * @return Parámetro que devuelve el contenido de el parámetro privado de correo Para Cliente
     */
    public String getCorreo() {
        return correo;
    }
    /** Método Set de Correo
     *
     * @param correo Parámetro que se encarga de enviarse para ser el nuevo valor de Correo
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }


    /** Método que tiene la función de almacenar los atributos del objeto en un string
     *
     * @return Devuelve un String con los atributos del objeto Cliente
     */
    public String toStringCliente() {
        return "Cliente2{" +
                "nombre='" + nombre + '\'' +
                ", apellido1='" + apellido1 + '\'' +
                ", apellido2='" + apellido2 + '\'' +
                ", direccion='" + direccion + '\'' +
                ", correo='" + correo + '\'' +
                '}';
    }//FIN TOSTRING



}//FIN CLIENTE2



