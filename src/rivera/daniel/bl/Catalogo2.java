package rivera.daniel.bl;


/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 */

public class Catalogo2 {
    private String mes = "", fecha = "";
    private String year = "";

    /**
     * Constructor por defecto de Catálogo2
     */
    public Catalogo2() {

    }//FIN

    /**
     * Constructor que recibe todos los parámetros de Catalogo2 y los Inicializa
     * @param mes
     * @param fecha
     * @param year
     */

    public Catalogo2(String mes, String fecha, String year) {
        this.mes = mes;
        this.fecha = fecha;
        this.year = year;
    }

    /** Método Get de Mes
     *
     * @return Parámetro que devuelve el contenido de el parámetro privado de Mes para Catálogo
     */
    public String getMes() {
        return mes;
    }//FIN

    /** Método Set de Mes
     *
     * @param mes Parámetro que se encarga de enviarse para ser el nuevo valor del Mes
     */
    public void setMes(String mes) {
        this.mes = mes;
    }//FIN
    /** Método Get de Fecha
     *
     * @return Parámetro que devuelve el contenido de el parámetro privado de fecha para catálogo
     */
    public String getFecha() {
        return fecha;
    }//FIN

    /** Método Set de fecha
     *
     * @param fecha Parámetro que se encarga de enviarse para ser el nuevo valor de fecha
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }//FIN

    /** Método Get de Año
     *
     * @return Parámetro que devuelve el contenido de el parámetro privado de año para catálogo
     */
    public String getYear() {
        return year;
    }//FIN
    /** Método Set de año
     *
     * @param year Parámetro que se encarga de enviarse para ser el nuevo valor del año
     */
    public void setYear(String year) {
        this.year = year;
    }

    /** Método que tiene la función de almacenar los atributos del objeto en un string
     *
     * @return Devuelve un String con los atributos del objeto Cliente
     */
    public String toStringCatalogo() {
        return "Catalogo2{" +
                "mes='" + mes + '\'' +
                ", fecha='" + fecha + '\'' +
                ", year=" + year +
                '}';
    }


}//FIN Catalogo

