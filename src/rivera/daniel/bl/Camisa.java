package rivera.daniel.bl;


/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 *
 */
public class Camisa {

    private String id;
    private String color, descripcion;
    private String size;
    private String precio;

    /**
     * Constructor por defecto de Camisa
     */
    public Camisa() {
    }

    /**
     * Constructor que recibe todos los parámetros de Camisa y los Inicializa
     * @param id Parámetro que simboliza el id de la camisa
     * @param color Parámetro que simboliza el color de la camisa
     * @param descripcion Parámetro que simboliza la descripcion de la camisa
     * @param size Parámetro que simboliza el tamaño de la camisa
     * @param precio Parámetro que simboliza el precio de la camisa
     */
    public Camisa(String id, String color, String descripcion, String size, String precio) {
        this.id = id;
        this.color = color;
        this.descripcion = descripcion;
        this.size = size;
        this.precio = precio;
    }

    /** Método Get de ID
     *
     * @return Parámetro que devuelve el contenido de el parámetro privado ID
     */
    public String getId() {
        return id;
    }

    /** Método set de ID
     *
     * @param id Parámetro para asignar contenido al parámetro privado ID
     */
    public void setId(String id) {
        this.id = id;
    }

    /** Método Get de Color
     *
     * @return Parámetro que devuelve el contenido de el parámetro privado ID
     */
    public String getColor() {
        return color;
    }

    /**  Método set de Color
     *
     * @param color Parámetro para asignar contenido al parámetro privado color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /** Método Get de descripción
     *
     * @return parámetro que devuelve el contenido de el parámetro privado Descripción
     */
    public String getDescripcion() {
        return descripcion;
    }

    /** Método set de Descrición
     *
     * @param descripcion Parámetro para asignar contenido al parámetro privado Descripción
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /** Método Get de tamaño
     *
     * @return parámetro que devuelve el contenido de el parámetro privado Size(tamaño)
     */
    public String getSize() {
        return size;
    }

    /** Método set de tamaño
     *
     * @param size Parámetro para asignar contenido al parámetro privado Size (tamaño)
     */
    public void setSize(String size) {
        this.size = size;
    }

    /** Método Get de precio
     *
     * @return parámetro que devuelve el contenido de el parámetro privado Precio
     */
    public String getPrecio() {
        return precio;
    }

    /**Método set de Precio
     *
     * @param precio Parámetro para asignar contenido al parámetro privado Precio
     */
    public void setPrecio(String precio) {
        this.precio = precio;
    }

    /** Método que tiene la función de almacenar los atributos del objeto en un string
     *
     * @return Devuelve un String con los atributos del objeto Camisa
     */
    public String toStringCamisa() {
        return "Camisa{" +
                "id=" + id +
                ", color='" + color + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", size=" + size +
                ", precio=" + precio +
                '}';
    }


}//FIN CAMISA
