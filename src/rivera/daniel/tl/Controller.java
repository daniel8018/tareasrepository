package rivera.daniel.tl;

import rivera.daniel.bl.Camisa;
import rivera.daniel.bl.Cliente2;
import rivera.daniel.bl.Catalogo2;
import rivera.daniel.dl.Capa;

import java.util.ArrayList;
import java.util.List;

/** @author Luis Daniel Rivera Calderón
 * @version 1.0.0
 * @since 1.0.0
 *
 */

public class Controller {
    private static Capa logica;


    /**
     * Constructor que recibe todos los parámetros de Controller  y los Inicializa
     * @param logica parámetro que indica el objeto proveniente de la capa lógica
     */

    public Controller(Capa logica) {
        this.logica = logica;

    }

    /*
    Constructor que inicializa el objeto Logica
     */
    public Controller(){
        logica = new Capa();
    }
    Cliente2 client = new Cliente2();


    //****************************************REGISTRAR*********************************************+

    /**
     * Metodo que tiene la función de Registrar la información del cliente y crear el objeto y enviarlo a la capa lógica
     * @param nombre parámetro que almacena el nombre del cliente
     * @param apellido1 parámetro que almacena el Primer apellido del cliente
     * @param apellido2 parámetro que almacena el Segundo Apellido del cliente
     * @param direccion parámetro que almacena la dirección del cliente
     * @param correo parámetro que almacena el correo del cliente
     */

    public void registrarCliente(String nombre,String apellido1, String apellido2,String direccion, String correo){

        Cliente2 obCliente = new Cliente2(nombre,apellido1,apellido2,direccion,correo);
        logica.agregarCliente(obCliente);

    }//FIN REGISTRARCLIENTE

    /**
     * Metodo que tiene la función de Registrar la información de la camisa y crear el objeto y enviarlo a la capa lógica
     * @param id  parámetro que almacena el id de la Camisa
     * @param color   parámetro que almacena el color de la Camisa
     * @param descripcion  parámetro que almacena la descripción de la Camisa
     * @param size   parámetro que almacena el tamaño de la Camisa
     * @param precio   parámetro que almacena el precio de la Camisa
     */

    public void registrarCamisa(String id, String color, String descripcion, String size, String precio){
        Camisa obCamisa = new Camisa(id,color,descripcion,size,precio);
        logica.agregarCamisa(obCamisa);
    }//FIN

    /**
     * Metodo que tiene la función de Registrar la información de la camisa y crear el objeto y enviarlo a la capa lógica
     * @param mes parámetro que almacena el mes del catálogo
     * @param fecha parámetro que almacena la fecha del catálogo
     * @param year parámetro que almacena el año del catálogo
     */
    public void registrarCatalogo(String mes, String fecha, String year){
        Catalogo2 obCatalogo = new Catalogo2(mes,fecha,year);
        logica.agregarCatalogo(obCatalogo);
    }//FIN
//****************************************VALIDAR*********************************************+

    /**Metodo que tiene la función de validar la información del cliente y enviar la información a la capa lógica
     *
     * @param pnombre  parámetro de nombre que es enviado a la capalóga para ser validado
     * @param papellido1  parámetro de primer apellido que es enviado a la capalóga para ser validado
     * @param papellido2  parámetro de segundo apellido que es enviado a la capalóga para ser validado
     * @param pdireccion parámetro de dirección que es enviado a la capalóga para ser validado
     * @param pcorreo  parámetro de correo que es enviado a la capalóga para ser validado
     * @return Envia los parámetros a la capa lógica
     *
     */

    public String ValidarClienteNombre(String pnombre,String papellido1, String papellido2,String pdireccion, String pcorreo){
        return logica.validarClienteNombre(pnombre, papellido1, papellido2, pdireccion, pcorreo);
    }//FIN


    /**Metodo que tiene la función de validar la información del catalogo y enviar la información a la capa lógica
     *
     * @param pmes parámetro de mes que es enviado a la capalóga para ser validado
     * @param pfecha parámetro de fecha que es enviado a la capalóga para ser validado
     * @param pyear parámetro de year que es enviado a la capalóga para ser validado
     * @return Envia los parámetros a la capa lógica
     */
    public String ValidarCatalogoNombre(String pmes, String pfecha, String pyear){
        return logica.validarCatalogoNombre(pmes,pfecha,pyear);
    }//FIN

    /**Metodo que tiene la función de validar la información de la camisa y enviar la información a la capa lógica
     *
     * @param pid parámetro de ID de la camisa que es enviado a la capalóga para ser validado
     * @param pcolor parámetro de color de la camisa que es enviado a la capalóga para ser validado
     * @param pdescripcion parámetro de descripcion de la camisa que es enviado a la capalóga para ser validado
     * @param psize parámetro de tamaño de la camisa que es enviado a la capalóga para ser validado
     * @param pprecio parámetro de precio de camisa que es enviado a la capalóga para ser validado
     * @return Envia los parámetros a la capa lógica
     */
    public String ValidarCamisaNombre(String pid, String pcolor, String pdescripcion, String psize, String pprecio){
        return logica.validarCamisaNombre(pid,pcolor,pdescripcion,psize, pprecio);
    }//FIN

    //******************************************LISTAR*******************************************+

    /**
     *
     * @return Envia la lista de Clientes a la capa lógica
     */
    public String[] listarClientes(){
        return logica.getCliente();
    }

    /**
     *
     * @return Envia la lista de Catalogos a la capa lógica
     */
    public String[] listarCatalogos(){
        return logica.getCatalogo();
    }

    /**
     *
     * @return Envia la lista de Camisas a la capa lógica
     */

    public String[] listarCamisas(){
        return logica.getCamisa();
    }





}//FIN CONTROLLER
